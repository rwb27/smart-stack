__version__ = '0.1.0'

import logging

# This code makes our module compatible with v2.10 and earlier extension loading
# It should be removable in due course
# Without the try: except block, this code will prevent the whole module from
# running without the microscope server present.  This is a problem if you want
# your code to be reusable outside of the OFM server.
try:
    from .ofm_extension import LABTHINGS_EXTENSIONS
except ModuleNotFoundError as e:
    if e.name.startswith("labthings") or e.name.startswith("openflexure_microscope"):
        # Silently ignore import errors from the OFM extension - it probably
        # just means we're not running as an OFM extension...
        pass
    else:
        raise e
finally:
    pass